/**
 * @name App
 * @version 1.0.0
 * @namespace
 * @description Aplicación principal
 */
var express = require('express');
var bodyParser = require('body-parser');
var fs = require('fs');
var requestJSON = require('request-json');

var globales = require('./globales');

var herramientas = require('./control/herram');
var usuarioControl = require('./control/usuario');
var cuentaControl = require('./control/cuenta');
var movimiControl = require('./control/movimi');

var app = express();
var port = process.env.PORT || 3000;
const URI = '/ibc/v1/';

var baseMLabURL = globales.baseMLabURL;
var apikeyMLab = globales.apikeyMLab;


// app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json({
  limit: '10mb'
}));


app.listen(port);
console.log('Escuchando en el puerto:' + port + '/tcp');


/** @method
 * @public
 * @name appSaludar
 * @summary Envia saludo.
 * @description Este método nos sirve de prueba para saber que la API está on line. <br>
 * Invocación -> [GET]/ibc/v1/
 * @example localhost:3000/ibc/v1/
 * @author IBC
 * @memberof App
 */
app.get(URI, herramientas.saludar);

/** @method @public
 * @name appHelp
 * @summary Despliega la página de ayuda.
 * @description Método que direcciona a la página de ayuda, que ha sido generada con JsDoc. <br>
 * Invocación -> [GET]/ibc/v1/help
 * @example localhost:3000/ibc/v1/help
 * @author IBC
 * @memberof App
 */
app.get(URI + 'help', herramientas.help);

/** @method @public
 * @name appTraerUsuarios
 * @summary Lista todos los usuarios.
 * @description Lista todos los usuarios, no se controla rol especial a los efectos de esta prueba.<br>
 * Invocación -> [GET]/ibc/v1/users
 * @example localhost:3000/ibc/v1/users
 * @author IBC
 * @memberof App
 */
app.get(URI + 'users', usuarioControl.traerUsuarios);

/** @method @public
 * @name appTraerUsuario
 * @summary Lista un usuario
 * @description Lista los datos de un usuario, controla titularidad.<br>
 * Invocación -> [GET]/ibc/v1/users/.id
 * @example localhost:3000/ibc/v1/users/114
 * @author IBC
 * @memberof App
 */
app.get(URI + 'users/:id', usuarioControl.traerUsuario);

/** @method @public
 * @name appCrearUsuario
 * @summary Alta de usuario
 * @description Crea un usuario, controla que no exista.<br>
 * Invocación -> [POST]/ibc/v1/users
 * @example localhost:3000/ibc/v1/users
 * @author IBC
 * @memberof App
 */
app.post(URI + 'users', usuarioControl.crearUsuario);

/** @method @public
 * @name appModificarUsuario
 * @summary Modicación de un usuario
 * @description Modifica un usuario, controla token.<br>
 * Invocación -> [PUT]/ibc/v1/users/.id
 * @example localhost:3000/ibc/v1/users/114
 * @author IBC
 * @memberof App
 */
app.put(URI + 'users/:id', usuarioControl.modificarUsuario);

/** @method @public
 * @name appBorrarUsuario
 * @summary Borrado de un usuario
 * @description Borra un usuario, controla rol especial.<br>
 * Invocación -> [DELETE]/ibc/v1/users/.id
 * @example localhost:3000/ibc/v1/users/108
 * @author IBC
 * @memberof App
 */
app.delete(URI + 'users/:id', usuarioControl.borrarUsuario);

/** @method @public
 * @name appLogin
 * @summary Login
 * @description Realiza el login de usuario.<br>
 * Invocación -> [POST]/ibc/v1/login
 * @example localhost:3000/ibc/v1/login
 * @author IBC
 * @memberof App
 */
app.post(URI + 'login', usuarioControl.loguearUsuario);

/** @method
 * @public
 * @name appCambiarContraseña
 * @summary Cambio de contraseña
 * @description Cambia la comtraseña de un asuario ya logueado.<br>
 * Invocación -> [PUT]/ibc/v1/cambiacontra
 * @example localhost:3000/ibc/v1/cambiacontra
 * @author IBC
 * @memberof App
 */
app.put(URI + 'cambiacontra', usuarioControl.cambiarContraUsuario);

/** @method @public
 * @name appLogout
 * @summary Logout
 * @description Desloguea a un asuario ya logueado.<br>
 * Invocación -> [POST]/ibc/v1/logout
 * @example localhost:3000/ibc/v1/logout
 * @author IBC
 * @memberof App
 */
app.post(URI + 'logout', usuarioControl.desloguearUsuario);

/** @method
 * @public
 * @name appRestablecerContraseña
 * @summary Restablecer contraseña
 * @description Restablece la contraseña de un usario existente, enviándole un email.<br>
 * Invocación -> [PUT]/ibc/v1/restacontra
 * @example localhost:3000/ibc/v1/restacontra
 * @author IBC
 * @memberof App
 */
app.put(URI + 'restacontra', usuarioControl.restablecerContraUsuario);



/** @method @public
 * @name appCrearCuenta
 * @summary Crea una nueva cuenta de un usuario.
 * @description Se controla que el usuario exista y se use un token válido para ese usuario. <br>
 * Invocación -> [POST]/ibc/v1/users/.id/cuentas
 * @example localhost:3000/ibc/v1/users/115/cuentas
 * @author IBC
 * @memberof App
 */
app.post(URI + 'users/:id/cuentas', cuentaControl.crearCuenta);

/** @method @public
 * @name appListarCuentas
 * @summary Lista cuentas de un usuario.
 * @description Se controla que el usuario esté logueado, y el ID de la consulta coincida con el del token. <br>
 * Invocación -> [GET]/ibc/v1/users/.id/cuentas
 * @example localhost:3000/ibc/v1/users/115/cuentas
 * @author IBC
 * @memberof App
 */
app.get(URI + 'users/:id/cuentas', cuentaControl.listarCuentas);

/** @method @public
 * @name appBorrarCuenta
 * @summary Borra una cuenta.
 * @description Se controla que el usuario esté logueado, y el ID del usuario coincida con el del token. <br>
 * Además se verifica que el saldo de la cuenta sea iguala cero y no existan movimientos para la misma.<br>
 * Invocación -> [DELETE]/ibc/v1/users/.id/cuentas
 * @example localhost:3000/ibc/v1/users/115/cuentas
 * @author IBC
 * @memberof App
 */
app.delete(URI + 'users/:id/cuentas/:idc', cuentaControl.borrarCuenta);


/** @method @public
 * @name appCrearMovimiento
 * @summary Crea un nuevo movimiento.
 * @description El movimiento (débito/crédito) es sobre una cuenta de un usuario.<br>
 * Se controla que la cuenta/usuario exista, que el token ea válido para ese usuario, y saldo exista suficiente.<br>
 * Invocación -> [POST]/ibc/v1/users/.id/cuentas/:idc/movimientos
 * @example localhost:3000/ibc/v1/users/115/cuentas/28/movimientos
 * @author IBC
 * @memberof App
 */
app.post(URI + 'users/:id/cuentas/:idc/movimientos', movimiControl.crearMovimi);

/** @method @public
 * @name appListarMovimientos
 * @summary Listar movimientos.
 * @description El movimiento (débito/crédito) es sobre una cuenta de un usuario.<br>
 * Se controla que cuenta/usuario exista, se verifica token válido para ese usuario.<br>
 * Invocación -> [GET]/ibc/v1/users/:id/cuentas/:idc/movimientos
 * @example localhost:3000/ibc/v1/users/115/cuentas/28/movimientos
 * @author IBC
 * @memberof App
 */
app.get(URI + 'users/:id/cuentas/:idc/movimientos', movimiControl.listarMovimi);
