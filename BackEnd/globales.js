/**
 * @global
 * @name Globales
 * @description Definición de valores globales de toda la API
 * @module
 */

/** @constant */
const baseMLabURL = "https://api.mlab.com/api/1/databases/techubduruguay/collections/";
/** @constant */
const apikeyMLab = "apiKey=SB0IVqp6dnAjQYfIwpa-nEzFYmN2SYr9";
/** @constant */
const meses = new Array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
/** @constant */
const diasSemana = new Array("Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado");
/** @constant */
const helpURL = "http://localhost/~ivan/hack-ivan";
/** @constant */
const emailHost = "smtp.mailtrap.io";
/** @constant */
const emailPort = 2525;
/** @constant */
const emailUser = "19ec7afc4fea51";
/** @constant */
const emailPass = "532a7f130af6d0";
/** @constant */
const emailFrom = 'bentancorivan@gmail.com';


module.exports = {
  baseMLabURL: baseMLabURL,
  apikeyMLab: apikeyMLab,
  meses: meses,
  diasSemana: diasSemana,
  helpURL: helpURL,
  emailHost,
  emailPort,
  emailUser,
  emailPass,
  emailFrom
}
