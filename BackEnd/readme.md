API hack-ivan @versión 1.0.0 / diciembre 2018

** API REST / Practitioner Ninja - Uruguay 2018 **


Mediante esta API se pueden administrar los servicios bancarios de usuarios, cuentas, y movimientos

            ..........(0 0)
            .---oOO--- (_)-----.
          ╔═════════════════════════════════════╗
          ║  Practitioner  - BBVA Uruguay 2018  ║
          ╚═════════════════════════════════════╝
            '----------------------oOO
            ........|__|__|
            ........ || ||
            ....... ooO Ooo



@Iván Bentancor (2018)
<br><img src="./250px-GPLv3_Logo.svg.png" alt="GPLv3" style="width:95px;height:46px;">
